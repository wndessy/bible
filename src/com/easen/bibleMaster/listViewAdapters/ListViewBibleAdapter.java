
package com.easen.bibleMaster.listViewAdapters;

import java.util.ArrayList;

import com.example.biblemaster.R;
import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ListViewBibleAdapter extends ArrayAdapter<ListViewBibleBean> {

	private ArrayList<ListViewBibleBean>	mylist		= new ArrayList<ListViewBibleBean>();
	Activity											myActivity	= null;

	public ListViewBibleAdapter(Context applicationContext, Activity activity, ArrayList<ListViewBibleBean> alist) {
		super(applicationContext, R.layout.listviebible, alist);
		mylist = alist;
		myActivity = activity;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View itemView = convertView;
		// make sure we have a view to work with
		if (itemView == null) {
			itemView = myActivity.getLayoutInflater().inflate(R.layout.listviebible, parent, false);
		}
		// find the item to work with
		ListViewBibleBean lableImageView = mylist.get(position);
		// fill the view
		ImageView imageView = (ImageView) itemView.findViewById(R.id.icon_arrow_forward);
		imageView.setImageResource(lableImageView.getImageLocation());
		TextView textView = (TextView) itemView.findViewById(R.id.tv_itemName);
		textView.setText(lableImageView.getLabel());
		return itemView;
	}

}
