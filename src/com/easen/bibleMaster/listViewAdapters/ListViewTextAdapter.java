
package com.easen.bibleMaster.listViewAdapters;

import java.util.ArrayList;

import com.example.biblemaster.R;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class ListViewTextAdapter extends ArrayAdapter<ListViewVersebean> {

	private ArrayList<ListViewVersebean>	mylist		= new ArrayList<ListViewVersebean>();

	private Activity							myActivity	= null;

	public ListViewTextAdapter(Context applicationContext, Activity activity, ArrayList<ListViewVersebean> alist) {
		super(applicationContext, R.layout.listviewverse, alist);
		mylist = alist;
		myActivity = activity;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View itemView = convertView;
		// make sure we have a view to work with(may have been given null)
		if (itemView == null) {
			itemView = myActivity.getLayoutInflater().inflate(R.layout.listviewverse, parent, false);
		}
		// find the item to work with
		ListViewVersebean textviewitem = mylist.get(position);
		// fill the view
		TextView textView = (TextView) itemView.findViewById(R.id.verseText);
		textView.setText(textviewitem.getVerseId()+"-"+textviewitem.getText());
		return itemView;
	}

}
