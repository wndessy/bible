package com.easen.bibleMaster.listViewAdapters;

public class ListViewBibleBean {
	private String bookId;
	private String label;
	private int imageLocation;

	public ListViewBibleBean(String bookId, String label) {
		super();
		this.bookId = bookId;
		this.label = label;
	}

	public String getBookId() {
		return bookId;
	}

	public void setBookId(String bookId) {
		this.bookId = bookId;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public int getImageLocation() {
		return imageLocation;
	}

	public void setImageLocation(int imageLocation) {
		this.imageLocation = imageLocation;
	}

}
