package com.easen.bibleMaster.listViewAdapters;


public class ListViewVersebean {
	private String text;
	private String verseId;
	private String verseUniversalId;
	
	
	
	public ListViewVersebean(String text, String verseId) {
		super();
		this.text = text;
		this.verseId = verseId;
	}

	public String getText() {
		return text;
	}
	
	public void setText(String text) {
		this.text = text;
	}
	
	public String getVerseId() {
		return verseId;
	}
	
	public void setVerseId(String verseId) {
		this.verseId = verseId;
	}
	
	public String getVerseUniversalId() {
		return verseUniversalId;
	}
	
	public void setVerseUniversalId(String verseUniversalId) {
		this.verseUniversalId = verseUniversalId;
	}
	
	

}
