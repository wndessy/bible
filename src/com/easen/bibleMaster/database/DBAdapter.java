
package com.easen.bibleMaster.database;

import java.io.IOException;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class DBAdapter {

	protected static final String	TAG	= "DataAdapter";

	private final Context			mContext;

	private SQLiteDatabase			mDb;

	private DataBaseHelper			mDbHelper;

	public DBAdapter(Context context) {
		this.mContext = context;
		mDbHelper = new DataBaseHelper(mContext);
		open();

	}

	public DBAdapter createDatabase() throws SQLException {
		try {
			mDbHelper.createDataBase();
		} catch (IOException mIOException) {
			Log.e(TAG, mIOException.toString() + "  UnableToCreateDatabase");
			throw new Error("UnableToCreateDatabase");
		}
		return this;
	}

	public DBAdapter open() throws SQLException {
		try {
			mDbHelper.openDataBase();
			mDbHelper.close();
			mDb = mDbHelper.getReadableDatabase();
		} catch (SQLException mSQLException) {
			Log.e(TAG, "" + mSQLException.toString());
			throw mSQLException;
		}
		return this;
	}

	public void close() {
		mDbHelper.close();
	}

	public Cursor executeQuery(String sql) {
		Cursor cursor = null;
		try {
			if (mDb != null) {
				cursor = mDb.rawQuery(sql, null);
				if (cursor != null) {
					cursor.moveToNext();
					//cursor.moveToFirst();
				}
			} else {
				Log.i(TAG, "===========Db empty=========");
			}
		} catch (SQLException mSQLException) {
			Log.e(TAG, "==============" + mSQLException.toString());
			throw mSQLException;
		}
		return cursor;
	}



}