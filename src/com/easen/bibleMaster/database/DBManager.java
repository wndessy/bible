package com.easen.bibleMaster.database;

import java.security.PublicKey;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

public class DBManager extends SQLiteOpenHelper {

	public static final String DATABASE_NAME = "Bible.db";
	public static final String TABLE1_NAME = "books";
	public static final String col1 = "bookid INTEGER  PRIMARY KEY AUTOINCREMENT";
	public static final String col2 = "bookName TEXT";
	public static final String col3 = "bookText TEXT";
	public static final String TABLE2_NAME = "chapter";
	public static final String col4 = "bookId INTEGER";
	public static final String col5 = "chapterId INTEGER  PRIMARY KEY";
	public static final String col6 = "chapterText";
	public static final String TABLE3_NAME = "Verser";
	public static final String col7 = "chapterId INTEGER";
	public static final String col8 = "verseId INTEGER  PRIMARY KEY";
	public static final String col9 = "verseText TEXT";

	public DBManager(Context context) {
		super(context, DATABASE_NAME, null, 1);
		SQLiteDatabase db = this.getWritableDatabase();
	}

	public static abstract class tablecols implements BaseColumns {
		public static final String col1 = "bookid ";
		public static final String col2 = "bookName ";
		public static final String col3 = "bookText ";
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE IF NOT EXISTS " + TABLE1_NAME + "(" + col1
				+ "," + col2 + "," + col3 + ")");
		db.execSQL("CREATE TABLE IF NOT EXISTS " + TABLE2_NAME + "(" + col4
				+ "," + col5 + "," + col6 + ")");
		db.execSQL("CREATE TABLE IF NOT EXISTS " + TABLE2_NAME + "(" + col7
				+ "," + col8 + "," + col9 + ")");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXHISTS " + TABLE1_NAME);
		db.execSQL("DROP TABLE IF EXHISTS " + TABLE2_NAME);
		db.execSQL("DROP TABLE IF EXHISTS " + TABLE3_NAME);
		onCreate(db);
	}

	public Boolean insertData() {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues contentValues = new ContentValues();
		contentValues.put(col2, "Mathew ");
		contentValues.put(col3, "the first book of the new testament");
		long result = db.insert(TABLE1_NAME, null, contentValues);
		if (result == -1) {
			return false;
		}
		return true;
	}

}
