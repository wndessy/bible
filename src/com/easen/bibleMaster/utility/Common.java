package com.easen.bibleMaster.utility;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;


public  class Common {
	
	private static void moveTonewActivity(Class myclass,Context applicationContext,Activity activity) {
		Intent intent = new Intent(applicationContext, myclass);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		activity.startActivity(intent);
		activity.finish();
	}
}

