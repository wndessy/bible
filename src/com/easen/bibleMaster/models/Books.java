package com.easen.bibleMaster.models;

public class Books {
	private String genre;
	private String testament;
	private String name;
	private String book;

	public Books(String genre, String testament, String name, String book) {
		super();
		this.genre = genre;
		this.testament = testament;
		this.name = name;
		this.book = book;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public String getTestament() {
		return testament;
	}

	public void setTestament(String testament) {
		this.testament = testament;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBook() {
		return book;
	}

	public void setBook(String book) {
		this.book = book;
	}

}
