
package com.example.biblemaster;

import java.util.ArrayList;

import com.easen.bibleMaster.database.DBAdapter;
import com.easen.bibleMaster.listViewAdapters.ListViewTextAdapter;
import com.easen.bibleMaster.listViewAdapters.ListViewVersebean;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
public class Verse extends Activity {

	public static final String					BOOKVERSION	= "t_asv";

	private ArrayList<ListViewVersebean>	mylist		= new ArrayList<ListViewVersebean>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_verse);
		getVerses();
		populateListView();
	}

	private void getVerses() {
		Intent intent = getIntent();
		String bookid = intent.getStringExtra("bookId");
		String chapterid = intent.getStringExtra("chapterId");
		DBAdapter adapter = new DBAdapter(getApplicationContext());
		String sql = "select * from " + BOOKVERSION + " where b=" + bookid + " and c =" + chapterid;

		//String sql ="select  * from t_asv where b='28' and c='1'";
		Cursor cursor = adapter.executeQuery(sql);
		if(cursor!=null && cursor.getCount()>0){
			//Toast.makeText(getApplicationContext(), "Empty verse "+BOOKVERSION + " - "+ bookid+" - "+chapterid,Toast.LENGTH_LONG).show();

		do {
			mylist.add(new ListViewVersebean(cursor.getString(cursor.getColumnIndex("t")), cursor.getString(cursor.getColumnIndex("v"))));
		} while (cursor.moveToNext());
		cursor.close();
		adapter.close();
	}
	}

	private void populateListView() {
		ArrayAdapter<ListViewVersebean> myadapter = new ListViewTextAdapter(getApplicationContext(), Verse.this, mylist);
		ListView listView = (ListView) findViewById(R.id.lvVerse);
		listView.setAdapter(myadapter);
		registerOnItemClickListener(listView);
		registerOnItemLongClickListener(listView);
	}

	public void registerOnItemClickListener(ListView listView) {
		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

			}
		});
	}

	public void registerOnItemLongClickListener(ListView listView) {
		listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View v, int position, long id) {
				createPopupWindow();
				return true;
			}
		});
	}

	protected void createPopupWindow() {
		LayoutInflater layoutInflater = (LayoutInflater) getBaseContext().getSystemService(LAYOUT_INFLATER_SERVICE);
		View popupView = layoutInflater.inflate(R.layout.activity_popup_verse, null);
		final PopupWindow popupWindow = new PopupWindow(popupView, 4000, 4000);
		popupWindow.setBackgroundDrawable(getWallpaper());
		popupWindow.showAtLocation(new LinearLayout(this), Gravity.CENTER, 0, 0);
		popupWindow.update(0, 50, 300, 300);

		Button btnBookmark = (Button) popupView.findViewById(R.id.bookmark);
		Button btnFavourite = (Button) popupView.findViewById(R.id.favourite);
		Button btnShare = (Button) popupView.findViewById(R.id.share);

		btnBookmark.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {
				bookmarkVerse();
				popupWindow.dismiss();
			}
		});
		btnFavourite.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {
				favouriteVerse();
				popupWindow.dismiss();
			}
		});
		btnShare.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {
				shareVerse();
				popupWindow.dismiss();
			}
		});
		// popupWindow.showAsDropDown(btnOpenPopup, 50, -30);

	}

	protected void shareVerse() {

	}

	protected void favouriteVerse() {

	}

	protected void bookmarkVerse() {

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.verse, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	//to hadle back button for android 2.0 and above
		@Override
		public void onBackPressed() {
			moveTonewActivity( Chapter.class);
		}
		//to hadle back button for android below 2.0

		@Override
		public boolean onKeyUp(int keyCode, KeyEvent msg) {
			switch (keyCode) {
			case (KeyEvent.KEYCODE_BACK):
				moveTonewActivity(Chapter.class);
				return true;
			}
			return false;
		}
		private void moveTonewActivity(Class myclass) {
			Intent intent = new Intent(getApplicationContext(), myclass);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			finish();
		}
	
	
}
