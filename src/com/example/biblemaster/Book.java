
package com.example.biblemaster;

import java.util.ArrayList;

import com.easen.bibleMaster.database.DBAdapter;
import com.easen.bibleMaster.listViewAdapters.ListViewBibleAdapter;
import com.easen.bibleMaster.listViewAdapters.ListViewBibleBean;
import com.easen.bibleMaster.models.Books;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class Book extends Activity implements OnClickListener {

	private ArrayList<ListViewBibleBean>	mylist	= new ArrayList<ListViewBibleBean>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		populateBooksList();
		// populatearayList();
		populateListView();
	}

	private void populateBooksList() {
		DBAdapter mDbHelper = new DBAdapter(getApplicationContext());
		mDbHelper.createDatabase();
		mDbHelper.open();
		String sql = "SELECT * FROM key_english";
		Cursor testdata = mDbHelper.executeQuery(sql);
		do {
			mylist.add(new ListViewBibleBean(testdata.getString(testdata.getColumnIndex("b")), testdata.getString(testdata.getColumnIndex("n"))));

		} while (testdata.moveToNext());

	}

	private void populateListView() {
		ArrayAdapter<ListViewBibleBean> myadapter = new ListViewBibleAdapter(getApplicationContext(), Book.this, mylist);
		ListView listView = (ListView) findViewById(R.id.lvBooks);
		listView.setAdapter(myadapter);
		registerOnItemClickListener(listView);
	}

	public void registerOnItemClickListener(ListView listView) {
		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Intent intent = new Intent(getApplicationContext(), Chapter.class);
				intent.putExtra("bookId", mylist.get(position).getBookId());
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivityForResult(intent, 1);
				finish();
				 Toast.makeText(getApplicationContext(), "hellow : " +
				 mylist.get(position).getBookId(), Toast.LENGTH_SHORT).show();
			}
		});
	}


	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

	}
}
