
package com.example.biblemaster;

import com.easen.bibleMaster.database.DBAdapter;
import com.easen.bibleMaster.database.DBManager;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.Toast;

public class Chapter extends Activity {

	public static final String	BOOKVERSION	= "t_asv";

	public static String			bookId		= "1";

	public static String				chapterId="";

	public DBManager				mydb;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_chapter);
		getChapters();
		populateButtons();
	}

	private int getChapters() {
		Intent intent = getIntent();
		bookId = intent.getStringExtra("bookId");
		DBAdapter adapter = new DBAdapter(getApplicationContext());
		String sql = "select distinct c  from " + BOOKVERSION + " where b=" + bookId;
		// Cursor cursor = adapter.getTestData();
		Cursor cursor = adapter.executeQuery(sql);
		int Totalchapters = cursor.getCount();
		cursor.close();
		adapter.close();
		return Totalchapters;
	}

	private void populateButtons() {
		int totalChapters = getChapters();
		int totalRows = determineNoOfRows(totalChapters);
		int counter = 1;
		TableLayout table = (TableLayout) findViewById(R.id.tableChapters);
		// draw rows
		for (int row = 0; row < totalRows; row++) {
			TableRow myrow = new TableRow(getBaseContext());
			myrow.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.FILL_PARENT, TableLayout.LayoutParams.WRAP_CONTENT));
			table.addView(myrow);
			// draw columns
			for (int collumn = 0; collumn < 4; collumn++) {
				Button button = new Button(getApplicationContext());
				button.setText("" + counter);
				counter++;
				myrow.addView(setOnclickListnerDynamic(button));
				if (counter > totalChapters) {
					break;
				}
			}
		}
	}

	private int determineNoOfRows(int totalChapters) {
		// determine the number of rows
		int rowDeterminer = totalChapters;
		int modulo = totalChapters % 4;
		if (modulo > 0) {
			if (modulo == 1) {
				rowDeterminer += 3;
			} else {
				rowDeterminer = totalChapters + (totalChapters % 4);
			}
		}
		// divide by 4 to ensure each row has 4 buttons
		return rowDeterminer / 4;
	}

	public Button setOnclickListnerDynamic(Button button) {
		chapterId=button.getText().toString();
		button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Toast.makeText(getApplicationContext(), "chapter -"+ chapterId, Toast.LENGTH_SHORT).show();
				
				Intent intent = new Intent(getApplicationContext(), Verse.class);
				intent.putExtra("bookId", bookId);
				intent.putExtra("chapterId",chapterId);
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
				finish();
				// mydb.insertData();
			}
		});
		return button;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.chapter, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	//to hadle back button for android 2.0 and above
	@Override
	public void onBackPressed() {
		moveTonewActivity( Book.class);
	}
	//to hadle back button for android below 2.0

	@Override
	public boolean onKeyUp(int keyCode, KeyEvent msg) {
		switch (keyCode) {
		case (KeyEvent.KEYCODE_BACK):
			moveTonewActivity( Book.class);
			return true;
		}
		return false;
	}
	private void moveTonewActivity(Class myclass) {
		Intent intent = new Intent(getApplicationContext(), myclass);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(intent);
		finish();
	}
	
}
