package com.example.biblemaster;

public class Contact {
	private int Id;
	private String Name;
	private String PhoneNumber;

	public Contact(int ID, String name, String phoneNumber) {
		super();
		this.Id = ID;
		Name = name;
		PhoneNumber = phoneNumber;
	}

	public Contact() {
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return Id;
	}

	public void setId(int id) {
		Id = id;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getPhoneNumber() {
		return PhoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		PhoneNumber = phoneNumber;
	}

}
