package com.example.biblemaster;

public class verseId {
	private int Id;
	private String text;

	public verseId(int id, String text) {
		super();
		Id = id;
		this.text = text;
	}

	public int getId() {
		return Id;
	}

	public void setId(int id) {
		Id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

}
